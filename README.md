# HW1

Dataset details

## Goals

The goal of this homework assignment is to describe the details of the dataset chosen for this course. Homework must be completed in Markdown, pushed to a private GitLab repository, rendered to PDF, and then saved back into the repository. A zip file of the repository (including markdown, PDF, any other work files, and exclusing the .git directory) must be submitted for peer grading.

## Questions

1. Basics
    1. What is the dataset name?
    2. How was it collected? Describe instruments, experiments, subjects, etc.
    3. Where did it come from?
    4. If pubilc, what is the URL to the dataset?
    5. Is there a paper associated with this dataset? Provide the link if so.
    6. Provide a short description of the dataset.
2. File Format
    1. What file format is the data distributed in? Provide a few sentence description of this file format.
    3. Provide an overview of the metadata associated with this dataset.
3. Structure & Encoding
    1. What is the structure of the dataset? Describe the dimensions, organization, hierachies, etc. Be reasonably detailed so that a reader could reconstruct the structure of the dataset. If there are multiple data structures in the dataset, describe each one.
    2. What is the data encoding model of the dataset? Be descriptive enough so that the explanation goes down to level of the three primitive data classes (integers, characters, and floating-point numbers). 
4. Size
    1. What is the size of the dataset as distributed?
    2. Is the dataset distributed with compression? If so, what compression is used and what is the uncompressed size of the dataset.
    3. Approximate the uncompressed size of the dataset using your understanding of the dataset structure and the encoding model.
        - The math here must be at the level of the three primitive data classes.
        - Provide a web screenshot or console text snippet (use `du` with the appropriate flags for calculating size of a directory, `-sch` or `-scbh` may be helpful here) of the size of the distributed dataset
        - If compression is used, also show the uncompressed dataset size, as that is what is being estimated
        - Video and audio files can be compressed without being put in zip/gzip files, be careful!
5. Workup
    1. What type of analysis might you hope to perform with this data?
    2. Describe potential summary statistics or measures that may be appropriate to calculate
    3. What types of visualizations might you create with this data?
