# 1. Dataset Properties
1. The dataset name is the NIH Collection of Chest X-rays. 
2. It was collected via X-ray of patients that have come into hospitals across the United States.  
3. It came from the NIH - National Institute of Health
4. The url is https://www.kaggle.com/nih-chest-xrays/data. 
5. The paper linked to the dataset is "ChestX-ray8: Hospital-scale Chest X-ray Database and Benchmarks on Weakly-Supervised Classification and Localization of Common Thorax Diseases." (Wang et al.) The url is https://openaccess.thecvf.com/content_cvpr_2017/papers/Wang_ChestX-ray8_Hospital-Scale_Chest_CVPR_2017_paper.pdf The full citation is Wang X, Peng Y, Lu L, Lu Z, Bagheri M, Summers RM. ChestX-ray8: Hospital-scale Chest X-ray Database and Benchmarks on Weakly-Supervised Classification and Localization of Common Thorax Diseases. IEEE CVPR 2017,
6. NIH Chest X-ray Dataset is comprised of 112,120 X-ray images with disease labels from 30,805 unique patients. There are 15 classes of diseases that are to be classified based on the X-ray images. Atelectasis, Consolidation, Infiltration, Pneumothorax, Edema, Emphysema, Fibrosis, Effusion, Pneumonia, Pleural_thickening, Cardiomegaly, Nodule Mass, and Hernia.
# 2. File Format
1. The file format is given in images (.png) files. PNG (Portable Network Graphics) is a raster-graphics file format that supports lossless data compression, built specifically for transferring images across the internet. Due to that, palettes such as 24-bit RGB, grayscale, with our without alpha encoding are supported, but print palettes such as CMYK is not. A PNG contains a single image in an structure of chunks, which encode the baisc pixels and other information. There are 5 channels for PNG - which inlude grayscale, rgb/truecolor, indexed channel which goes into a palette of colors, grayscale and alpha where alpha determines the opacity of the each pixel, and RGBA (with alpha). Due to the presence of alpha PNGs are available to be transparent. Furthermore, the compression algorithm DEFLATE, used by the PNG is a lossless data compression algorithm which is non-patented. Hence, PNGs are known for the ease of portability, completeness, losslessness, efficiency (due to compression), ease of use, interchangeability, flexibility, and is non-propietary. 
2. The Metadata for each image includes its image index, Labels, Follow-up #, Patient ID, Patient Age, Patient Gender, View Position, Original Image Size and Original Pixel Spacing.   

# 3. Structure and Encoding
1. Within the home directory of the dataset, There are 12 subdirectories, labeled with the index of the folder (1,2,3..12), each folder has 1 folder labeled images  within it. Within each of the single folders, approximately 10000 png images of the Chest X-rays are stored. The png files are organised by the patient id and the followup visit number, with the initial visit initialised at 0. Therefore, the names are <patientid_followupvisit#>. Furthermore, there is a .csv file (Data_Entry_2017) which contains the metadata of all the 112,120 chest x-ray images. 
2. The chest x-rays are PNG files of size 1024 x 1024 pixels, which are a compressed version of the DICOM data object that the X-rays were in their native format. DICOM grayscale standard display function is the greyscale encoding in DICOM that allows X-rays to be viewed the same across all medical devices. The PNG encoding is done through two steps - Filtering, and then Compression. Delta encoding, where the difference between the previous value is stored, is used as the filtering step. Delta Encoding is powerful; if the data is linearly correlated like a gradient of an image, then the values in the Delta encoding are very small as a result, which makes it easier to compress. PNG is filtered via taking some relation to the pixel to the left, the pixel above,and the pixel above-left, which are all integers. The greyscale on the PNG is encoded with a bit depth of 8 as found out from the file command (A test file is uploaded to this repository X-ray.png). After the filtering, the image is compressed using the DEFLATE Algorithm.

+ Console text snippet
`----@rice16:~/hw1$ file "X-ray.png"
X-ray.png: PNG image data, 1024 x 1024, 8-bit grayscale, non-interlaced`

# 4. Size
1. The distribution size is 41.92 GB, as a zip format. 
2. The dataset is compressed via DEFLATE Compression, distributed as a zip archive file format, which is roughly 62 percent compression. We should expect a 42/0.62 ~ 68 GB for the uncompressed dataset.
1.The uncompressed size of the distributed dataset is 64 GB.
+ Console text snippet
`sohams@rice16:~$ du -c -s -h 
data.zip 64G
data.zip 64G total`
This is reasonable. 

3. The true uncompressed size of the data set. 
We know that that each of the png images are 1024 x 1024 which is 1048576 pixels.
Each pixel is encoded with 8 bits - so 1048576*8 = 8388608 bits. 
There are 112,120 images - therefore 112,120 * 8388608 = 940530728960 bits, converting 8 bits = 1 byte. 
There is 112 GB uncompressed size of the data set.

 

# 5. Workup  

1. The data was given as an opportunity to perform machine learning to develop
Convolutional Neural Networks to correctly classify each X-ray image as one of the 15 disease classes.
Beyond Deep Learning, I also think that developing an idea of how to humanly
classify chest-x-rays would be an interesting personal goal as I work with this dataset. In particular, if I can create a decision heirarchial tree that is based on 
X-ray physical spots and classify them, that would be pretty cool. Furthermore,
I think seeing how progression of a certain disease occurs in the followup x-rays would be an interesting idea to consider as well, in particular looking at the how the pixel brightness dims or increases in certain areas as infection progresses or regresses. 
Furthermore, the authors had to use a limited section of the chest X-rays to perform their supervised learning, I hope to increase the bounds of the box filter to use more of the X-ray and get a wider look. 

2. Some statistics worth looking into is the  average, median, and quartiles of  pixel brightness for the different disease classes, or see how the distribution of pixel brightness or pixel whiteness across various diseases to see which diseases cover up more of the X-ray. Furthermore, looking into how certain diseases are specifically clustered to a certain area (i.e calculate the spatial distribution of the greyscale across images). 
Other statistics worth looking into are how many average followup X-rays do we have that excluding the initial X-ray (the followup index of 0).
I also see creating a histogram of the number of followup visits made and X-rays taken. 


3. A prominent visualisation I plan to do is to create a Z-stack of X-rays that correspond to each of the 15 classified diseases. 
This would be a great visual to see how diseases are clustered in certain regions of the chest and which areas are more commonly affected than others, if any. 
Some other visualisations I see are the distributions of the pixel brightness/whiteness from the summary statistics given above. Other visualisations I imagine worth doing is creating a Z-stack of follo-up X-rays and visually seeing the prognosis of the disease, or lack there of for the cases where there is a followup.  

